import { Module } from '@nestjs/common';
import { FeatureUserApiModule } from '@nx-api/feature-user-api';

import { TypeOrmModule } from './modules/typeorm.module';

@Module({
  imports: [TypeOrmModule, FeatureUserApiModule],
})
export class CoreModule {}
