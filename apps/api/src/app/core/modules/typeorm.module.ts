import { TypeOrmModule as module, TypeOrmModuleOptions } from '@nestjs/typeorm';
import { UserEntity } from '@nx-api/feature-user-api';

import { environment } from '../../../environments/environment';

const config: TypeOrmModuleOptions = {
  ...(environment.database as TypeOrmModuleOptions),
  entities: [UserEntity],
};

export const TypeOrmModule = module.forRoot(config);
