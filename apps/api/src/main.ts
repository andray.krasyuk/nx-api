import { Logger } from '@nestjs/common';
import { NestFactory } from '@nestjs/core';
import { useContainer } from 'class-validator';

import { CoreModule } from './app/core/core.module';
import { environment } from './environments/environment';

async function bootstrap() {
  const app = await NestFactory.create(CoreModule);

  useContainer(app.select(CoreModule), { fallbackOnErrors: true });

  const globalPrefix = environment.server.prefix;
  const port = environment.server.port;

  app.setGlobalPrefix(globalPrefix);
  await app.listen(port);

  Logger.log(`🚀 Application is running on: http://localhost:${port}/${globalPrefix}`);
}

bootstrap();
