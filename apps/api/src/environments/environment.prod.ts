export const environment = {
  production: true,
  server: {
    prefix: 'api',
    port: 3000,
  },
  database: {
    type: 'sqlite',
    database: 'nx-api.sql',
    synchronize: false,
  },
};
