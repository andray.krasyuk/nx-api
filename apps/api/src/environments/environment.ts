export const environment = {
  production: false,
  server: {
    prefix: 'api',
    port: 3000,
  },
  database: {
    type: 'sqlite',
    database: 'nx-api.sql',
    synchronize: true,
  },
};
