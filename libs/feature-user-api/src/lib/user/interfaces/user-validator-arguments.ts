export interface UserValidatorArguments {
  mustExists: boolean;
  fieldName: string;
}
