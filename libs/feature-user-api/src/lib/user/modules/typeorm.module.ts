import { TypeOrmModule as module } from '@nestjs/typeorm';

import { UserRepository } from '../repositories/user.repository';

export const TypeOrmModule = module.forFeature([UserRepository]);
