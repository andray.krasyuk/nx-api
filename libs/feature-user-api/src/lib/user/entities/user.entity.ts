import { DatabaseTypes, ID, STRING } from '@nx-api/types-database';
import { Column, Entity, PrimaryGeneratedColumn } from 'typeorm';

import { Fields } from '../enums/fields.enum';
@Entity()
export class UserEntity {
  @PrimaryGeneratedColumn({ type: DatabaseTypes.ID })
  [Fields.Id]: ID;

  @Column({ type: DatabaseTypes.STRING, unique: true })
  [Fields.Name]: STRING;

  @Column({ type: DatabaseTypes.STRING, unique: true })
  [Fields.Email]: STRING;

  @Column({ type: DatabaseTypes.STRING })
  [Fields.Password]: STRING;
}
