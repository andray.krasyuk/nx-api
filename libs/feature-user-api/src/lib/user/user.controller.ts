import { Body, Controller, Delete, Get, Param, Patch, Post, UsePipes } from '@nestjs/common';

import { CreateUserDto } from './dto/create-user.dto';
import { FindParamsDto } from './dto/find-params.dto';
import { UpdateUserDto } from './dto/update-user.dto';
import { UserEntity } from './entities/user.entity';
import { Path } from './enums/path.enum';
import { ValidationPipe } from './pipes/validation.pipe';
import { UserService } from './user.service';

@UsePipes(ValidationPipe)
@Controller(Path.Core)
export class UserController {
  constructor(private readonly userService: UserService) {}

  @Post()
  create(@Body() createUserDto: CreateUserDto): Promise<UserEntity> {
    return this.userService.create(createUserDto);
  }

  @Get()
  findAll(): Promise<UserEntity[]> {
    return this.userService.findAll();
  }

  @Get(Path.Id)
  findOne(@Param() params: FindParamsDto): Promise<UserEntity> {
    return this.userService.findOne(params.id);
  }

  @Patch(Path.Id)
  update(@Param() params: FindParamsDto, @Body() updateUserDto: UpdateUserDto): Promise<UserEntity> {
    return this.userService.update(params.id, updateUserDto);
  }

  @Delete(Path.Id)
  remove(@Param() params: FindParamsDto): Promise<void> {
    return this.userService.remove(params.id);
  }
}
