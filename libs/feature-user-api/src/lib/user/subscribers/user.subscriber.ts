import { CryptService } from '@nx-api/feature-crypt';
import { Connection, EntitySubscriberInterface, EventSubscriber, InsertEvent } from 'typeorm';

import { UserEntity } from '../entities/user.entity';

@EventSubscriber()
export class UserSubscriber implements EntitySubscriberInterface<UserEntity> {
  constructor(private readonly connection: Connection, private readonly cryptService: CryptService) {
    this.connection.subscribers.push(this);
  }

  listenTo(): typeof UserEntity {
    return UserEntity;
  }

  async beforeInsert(event: InsertEvent<UserEntity>): Promise<void> {
    event.entity.password = await this.cryptService.hash(event.entity.password);
  }
}
