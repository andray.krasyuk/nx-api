import { IsEmail, Length } from 'class-validator';

import { UserExists } from '../decorators/user-exists.decorator';
import { Fields } from '../enums/fields.enum';

export class CreateUserDto {
  @Length(3, 24)
  @UserExists(false)
  [Fields.Name]: string;

  @IsEmail()
  @UserExists(false)
  [Fields.Email]: string;

  @Length(6, 28)
  [Fields.Password]: string;
}
