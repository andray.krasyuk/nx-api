import { IsNumberString } from 'class-validator';

import { UserExists } from '../decorators/user-exists.decorator';
import { Fields } from '../enums/fields.enum';

export class FindParamsDto {
  @IsNumberString()
  @UserExists()
  [Fields.Id]: number;
}
