import { OmitType, PartialType } from '@nestjs/mapped-types';

import { Fields } from '../enums/fields.enum';
import { CreateUserDto } from './create-user.dto';

export class UpdateUserDto extends PartialType(OmitType(CreateUserDto, [Fields.Email] as const)) {}
