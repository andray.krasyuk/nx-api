import { Injectable } from '@nestjs/common';
import { ID } from '@nx-api/types-database';

import { CreateUserDto } from './dto/create-user.dto';
import { UpdateUserDto } from './dto/update-user.dto';
import { UserEntity } from './entities/user.entity';
import { UserRepository } from './repositories/user.repository';

@Injectable()
export class UserService {
  constructor(private readonly userRepository: UserRepository) {}

  create(createUserDto: CreateUserDto): Promise<UserEntity> {
    return this.userRepository.save(createUserDto);
  }

  findAll(): Promise<UserEntity[]> {
    return this.userRepository.find();
  }

  findOne(id: ID): Promise<UserEntity> {
    return this.userRepository.findOne(id);
  }

  async update(id: ID, updateUserDto: UpdateUserDto): Promise<UserEntity> {
    return this.userRepository.save({ id, ...(await this.findOne(id)), ...updateUserDto });
  }

  async remove(id: ID): Promise<void> {
    await this.userRepository.delete(id);
  }
}
