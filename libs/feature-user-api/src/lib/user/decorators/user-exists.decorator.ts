import { registerDecorator } from 'class-validator';

import { UserExistsRule } from '../validators/user-exists.validator';

export function UserExists(mustExists = true, fieldName?: string) {
  return (object: unknown, propertyName: string) => {
    if (!fieldName) {
      fieldName = propertyName;
    }
    registerDecorator({
      name: 'UserExists',
      propertyName,
      target: object.constructor,
      validator: UserExistsRule,
      constraints: [mustExists, fieldName],
    });
  };
}
