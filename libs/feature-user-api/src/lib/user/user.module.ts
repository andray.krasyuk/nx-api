import { Module } from '@nestjs/common';
import { FeatureCryptModule } from '@nx-api/feature-crypt';

import { TypeOrmModule } from './modules/typeorm.module';
import { UserSubscriber } from './subscribers/user.subscriber';
import { UserController } from './user.controller';
import { UserService } from './user.service';
import { UserExistsRule } from './validators/user-exists.validator';

@Module({
  imports: [TypeOrmModule, FeatureCryptModule],
  controllers: [UserController],
  providers: [UserService, UserExistsRule, UserSubscriber],
})
export class UserModule {}
