import { ValidationPipe as Pipe, ValidationPipeOptions } from '@nestjs/common';

const options: ValidationPipeOptions = {
  whitelist: true,
};

export const ValidationPipe = new Pipe(options);
