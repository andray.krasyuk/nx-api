import { Injectable } from '@nestjs/common';
import { STRING } from '@nx-api/types-database';
import { ValidationArguments, ValidatorConstraint, ValidatorConstraintInterface } from 'class-validator';

import { UserValidatorArguments } from '../interfaces/user-validator-arguments';
import { UserRepository } from '../repositories/user.repository';
import { UserValidatorConstraints } from '../types/user-validator-constraints.type';

@Injectable()
@ValidatorConstraint({ name: 'UserExists', async: true })
export class UserExistsRule implements ValidatorConstraintInterface {
  constructor(private readonly userRepository: UserRepository) {}

  private getArguments(validationArguments: ValidationArguments): UserValidatorArguments {
    const args = validationArguments.constraints as UserValidatorConstraints;
    return {
      mustExists: args[0],
      fieldName: args[1],
    };
  }

  async validate(value: STRING, validationArguments?: ValidationArguments): Promise<boolean> {
    const { mustExists, fieldName } = this.getArguments(validationArguments);
    const user = await this.userRepository.findOne({ [fieldName]: value });
    if ((mustExists && !user) || (!mustExists && user)) {
      return false;
    }
    return true;
  }

  defaultMessage?(validationArguments?: ValidationArguments): string {
    const { mustExists, fieldName } = this.getArguments(validationArguments);
    if (mustExists) {
      return `User with this ${fieldName} does not exist`;
    }
    return `User with this ${fieldName} already exists`;
  }
}
