import { ColumnType, PrimaryGeneratedColumnType } from 'typeorm/driver/types/ColumnTypes';

export const DatabaseTypes = {
  ID: 'integer' as PrimaryGeneratedColumnType,
  STRING: 'varchar' as ColumnType,
};
