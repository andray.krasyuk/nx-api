import { Injectable } from '@nestjs/common';
import * as bcrypt from 'bcrypt';

import { SALT_ROUNDS } from './constants/crypt.constants';

@Injectable()
export class CryptService {
  hash(value: string): Promise<string> {
    return bcrypt.hash(value, SALT_ROUNDS);
  }

  validate(hash: string, data: string): Promise<boolean> {
    return bcrypt.compare(hash, data);
  }
}
