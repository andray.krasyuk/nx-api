import { Module } from '@nestjs/common';

import { CryptModule } from './crypt/crypt.module';

@Module({
  imports: [CryptModule],
  exports: [CryptModule],
})
export class FeatureCryptModule {}
